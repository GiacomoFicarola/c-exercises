#ifndef FUNCTIONS_H
#define FUNCTIONS_H

/*Functions declaration*/
int print(TableType); /* Function prototype for printing the table */
void printBus(TableType); /* Function for printing a specific bus with its related information */
unsigned int choice(); /* Function for displaying the user menu */
void AddNewBus(TableType *); /* Function for adding a new bus to the table */
void DeleteBus(TableType *); /* Function for deleting a bus */
void BookSeats(TableType *); /* Function for booking seats on a bus */
void ChangeTime(TableType *); /* Function for changing the departure time */
void LoadFromFile(TableType *); /* Function for loading the table from a file */
void SaveToFile(TableType); /* Function for saving the table to a file */

#endif