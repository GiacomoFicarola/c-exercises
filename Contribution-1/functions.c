#include "functions.h"

int print(TableType tab)/*I'm going to print the content of the table!*/
{
     int i;
     if(tab.BusesPresent <= 0) {/*Check if there is any bus in BusesPresent, if not, print nothing*/
          printf("\nOh, but there are no buses, what did you expect me to print?!?\n\n");
          return 1;/*return 1 to handle the error*/
     } else {
          printf("\nNow we will print all the buses!\n");
          for(i = 0; i < tab.BusesPresent; i++) {
               printf("\nThe bus %s, for %s, will depart at %d:%d, it has %d available seats\n", tab.Array[i].Code, tab.Array[i].destination, tab.Array[i].time.hour, tab.Array[i].time.minutes, tab.Array[i].availableSeats);
          } /*Print the bus table to the screen*/
          return 0;
     }
}

void printThatBus(TableType table) /* I'm going to search for a certain bus */
{
    int count = 0, i;
    char search[6];
    if(print(table) == 0){
        printf("Enter the code of the bus you are looking for: ");
        scanf("%s", search); /* I ask for the code so I can search for it */
        for(i = 0; i < table.BusesPresent; i++){
            if(strcmp(search, table.Array[i].Code) == 0){ /* If I find the searched code I increment count */
                count++;
            }
        }
        if(count == 0){ /* If count is equal to 0 the bus is not there */
            printf("\nThe searched bus is NOT in the table..\n\n");
        } else {
            printf("\nThe searched bus is in the table!\n\n"); /* Otherwise it is in the table */
        }
    }
    return;
}

unsigned int choice(void) {
     /*Prints the menu of choices that the user will see*/
     unsigned int chosen;
     /*Declare the number that will be selected by the user*/
     printf("%s","\nWelcome to Cotral s.p.a bus management\nChoose one of the following options:\n"  /*Print all the possible options for the user*/
                  "1 - Print all buses\n"
                  "2 - Add a bus\n"
                  "3 - Delete a bus from the table\n"
                  "4 - Print that bus\n"
                  "5 - Book seats on a bus\n"
                  "6 - Change bus departure time\n"
                  "7 - Load from a file\n"
                  "8 - Export to a file\n"
                  "9 - Terminate the program\n"
                  "What would you like to do :) ?");
     scanf("%u",&chosen);
     return chosen;
}

void AddNewBus(TipoTabella *tab)/*Insert a bus into the array, using syntactic sugar, each data is inserted into the correct array cell in the table*/
{
     int i;
     char buffer[6];
     printf("Let's proceed with the insertion of a bus into the table!\n");
     printf("Enter the identification code of the new bus (Max 5): ");
     scanf("%s", buffer);
     for(i = 0; i < tab->BusPresenti; i++) {
          if(strcmp(buffer, tab->Array[i].Cod) == 0) {
               printf("Bus with this code already exists\n");
               return;
          }
     }
     strcat(tab->Array[tab->BusPresenti].Cod, buffer);
     printf("Enter the destination of the bus (Max 30): ");
     scanf("%s", tab->Array[tab->BusPresenti].destinazione);
     printf("Enter the departure hour of the bus: ");
     scanf("%d", &tab->Array[tab->BusPresenti].orario.ora);
     printf("Enter the minutes: ");
     scanf("%d", &tab->Array[tab->BusPresenti].orario.minuti);
     printf("Enter the available seats on the bus: ");
     scanf("%d", &tab->Array[tab->BusPresenti].postiliberi);
     printf("\nBus insertion completed, thank you and see you next time!!\n \n");
     tab->BusPresenti += 1;
     return;
}

void DeleteBus(TableType *table) /* Function necessary for the deletion of a certain element */
{
     int i;
     char toDelete[6];
     printf("Welcome to the section dedicated to the deletion of an inserted bus.\n");
     if (print(*table) == 0) { /* I print the table only if the print function returns 0, that is, it has some content, if 1 it has nothing */
          printf("\nPlease enter the code of the bus you want to delete (Max 6): ");
          scanf("%s", toDelete); /* I ask for the code */
          for (i = 0; i < table->BusesPresent; i++) {
               if (strcmp(toDelete, table->Array[i].Code) == 0) { /* I perform the search and compare the code in the Array with the given one */
                    table->Array[i] = table->Array[table->BusesPresent]; /* If I find it, I replace the one found with the one that was in the last place */
                    printf("\nDeletion completed\n\n"); /* I have deleted */
               }
          }
     }
     table->BusesPresent -= 1; /* I reduce the size of the array */
     return;
}

void BookSeats(TableType *table)/*Function necessary for booking seats on a bus*/
{
     int num, i, var;
     char code[6];
     printf("Welcome to the section dedicated to booking seats on a bus.\n");
     if(print(*table) == 0) {
          printf("\nEnter the identification code of the bus: ");
          scanf("%s", code);/*Ask for the code to find the bus, it can be seen from the printout made one line above*/
          for(i = 0; i < table->BusesPresent; i++) {
               if(strcmp(code, table->Array[i].Code) == 0) {/*If the code is found..*/
                    printf("Enter the new number of seats to book on the bus: ");
                    scanf("%d", &num);
                    if(num <= table->Array[i].availableSeats) {
                         table->Array[i].availableSeats = table->Array[i].availableSeats - num;/*Change the number of seats on the bus*/
                         printf("\nSeats on the bus changed\n");
                    } else {
                         printf("\nYou are requesting too many seats!");/*If the requested seats are greater than the size of the bus*/
                    }
               }
          }
     }
     return;
}

void ChangeTime(TableType *table)/*Function necessary to change the departure time and minutes of a specific bus*/
{
     int i, hour, minutes, var; /*Local variables to take hour, minutes, and number, as well as the index*/
     char code[6];
     printf("\nWelcome to the section for changing the time of a bus.\n");
     if (print(*table) == 0) {
          printf("Enter the code of the bus whose time you want to change: ");
          scanf("%s", code); /*Take the identifying code of the bus*/
          for (i = 0; i < table->BusesPresent; i++) {
               if (strcmp(code, table->Array[i].Code) == 0) { /*Find it..*/
                    printf("Enter the new departure hour: ");
                    scanf("%d", &hour); /*Ask for the new hour*/
                    table->Array[i].time.hour = hour; /*Change it*/
                    printf("Time change completed!\n");
                    printf("Do you want to change the minutes as well? (0=Yes, 1=No): "); /*Ask if they want to change anything else*/
                    scanf("%d", &var);
                    if (var == 0) {
                         printf("Enter the new departure minutes: ");
                         scanf("%d", &minutes); /*Ask for the new minutes..*/
                         table->Array[i].time.minutes = minutes; /*Change them*/
                         printf("Minutes change completed!");
                    }
               } else {
                    printf("Bus not found..\n"); /*If the code is not found... tough luck*/
               }
          }
     }
}

void LoadFromFile(TableType *table)
{
     int h, g;
     char filename[51]; /* Array to store the file name */
     char buffer[6]; /* Temporary variable for checking the presence of duplicate buses in the table */
     printf("Enter the name of the file from which to load the bus table (Max 50): ");
     scanf("%s", filename);
     FILE *file; /* Pointer of type FILE, pointing to the file */
     int i, j, k, currentlyAllocated, allocatedFromFile = 0, num, MAXBUS = 10; /* Open the file in read mode */
     file = fopen(filename, "r");
     if (file == NULL) {
          printf("\nProblem opening file\n"); /* Exit and close everything */
          free(file);
     } else {
          /* Read the first line of the file where the number of buses in the file is stored */
          fscanf(file, "%d\n", &allocatedFromFile);
          if (allocatedFromFile == 0) { /* Check if there are buses in the file */
               printf("\nNo buses in the file\n");
               fclose(file); /* Close the file */
               free(file); /* Free the pointer */
          } else {
               if (allocatedFromFile > MAXBUS) { /* Check that the number of buses allocated in the file is not greater than the number of buses that can be allocated in the table */
                    printf("\nNot enough space to load buses into memory\n");
                    fclose(file); /* Close the file */
                    free(file);
               } else {
                    /* Save the number of currently allocated buses before the import */
                    currentlyAllocated = table->BusesPresent;
                    /* Check that the number of buses allocated in the file is not > than the number of buses that can be allocated at this time (MAXBUS - currentlyAllocated) */
                    if (allocatedFromFile > (MAXBUS - currentlyAllocated)) {
                         printf("\nNot enough space to add buses to memory\n");
                         fclose(file); /* Close the file */
                         free(file);
                    } else {
                         /* Start from the last allocated cell in the table + 1 using an index (i) and start adding the buses to the table using another index (k) that keeps track of those in the file */
                         for (i = currentlyAllocated, k = 0; k < allocatedFromFile; k++, i++) {
                              /* Read the bus data */
                              fscanf(file, "%s ", buffer);
                              for (h = 0; h < table->BusesPresent - 1; h++) {
                                   g = 0;
                                   if (strcmp(buffer, table->Array[h].Code) == 0) {
                                        printf("\nBus %s already present in memory\n", buffer);
                                        g = 1;
                                   }
                              }
                              if (g != 1) {
                                   strcat(table->Array[i].Code, buffer);
                                   fscanf(file, "%s ", &(table->Array[i].destination));
                                   fscanf(file, "%d ", &(table->Array[i].time.hour));
                                   fscanf(file, "%d ", &(table->Array[i].time.minutes));
                                   fscanf(file, "%d ", &(table->Array[i].availableSeats));
                              }
                         }
                         /* Update the number of allocated buses (those already present + those imported) */
                         table->BusesPresent = allocatedFromFile + currentlyAllocated;
                    }
               }
          }
          fclose(file); /* Close the file */
          free(file);
     }
     return;
}

void SaveToFile(TableType table)/*Receive the table*/
{
     char FileName[51];/*Declare the array to store the chosen file name*/
     printf("Enter the name of the file to export the entire Bus table (Max 50, any extension):");
     scanf("%s", FileName);
     FILE *file;/*Create the file pointer*/
     int i;
     file = fopen(FileName, "w");/*Open the file for writing*/
     if (file == NULL) {
          printf("\nProblem opening file\n");/*Report the problem in opening the file*/
     } else {
          /* Write the number of allocated buses so far in the first line of the file */
          fprintf(file, "%d\n", table.BusesPresent);
          for (i = 0; i < table.BusesPresent; i++) {
               fprintf(file, "%s ", table.Array[i].Code);/*Print the individual fields contained in the array to the file*/
               fprintf(file, "%s ", table.Array[i].destination);
               fprintf(file, "%d ", table.Array[i].time.hour);
               fprintf(file, "%d ", table.Array[i].time.minutes);
               fprintf(file, "%d\n", table.Array[i].availableSeats);  
          }
          printf("\nFile printing completed!\n");
     }
     fclose(file);/*Close the file pointer*/
     free(file);
     return;
}