#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "functions.h"

#define A 15

struct time {
     int hour;
     int minutes;
};

typedef struct time TimeType;

struct Bus {
     char Code[6];
     char destination[31];
     TimeType time;
     int availableSeats;
};

typedef struct Bus BusType;

struct Node {
     BusType Array[A];
     int BusesPresent;
};

typedef struct Node TableType;

int main(void)
{
     TableType tab;
     unsigned int choice; /*Declare a variable to store and verify the choice made in the choice() function*/
     tab.BusesPresent = 0; /*Value to count the inserted buses, it's in the struct*/
     while((choice = choice()) != 9){
          switch(choice){ /*Handle all the choice cases*/
               case 1:
                    print(tab); /*Print the bus table to the screen*/
                    break;
               case 2:
                    addNewBus(&tab); /*Add a new bus*/
                    break;
               case 3:
                    deleteBus(&tab); /*Delete a bus*/
                    break;
               case 4:/*Search and print a specific bus*/
                    printBus(tab);
                    break;
               case 5:/*Book seats on a bus*/
                    bookSeats(&tab);
                    break;
               case 6:/*Change the departure time*/
                    changeTime(&tab);
                    break;
               case 7:/*Load the bus table from a file*/
                    loadFromFile(&tab);
                    break;
               case 8:/*Save the bus table to a file*/
                    saveToFile(tab);
                    break;
               default:
                    puts("Incorrect choice!"); /*Unexpected choice*/
                    break;
          }
     }
     printf("\nThank you and goodbye!\n\n");
     system("PAUSE");
     return 0;
}