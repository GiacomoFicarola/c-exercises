#include "functions.h"  /*Include the header file with the function prototypes*/

void InitializeTree(TreeType *tree)
{
	*tree = NULL;
	return;
}

void PrintTreeInPreorder(TreeType a)
{
    /* As long as it finds contacts in the tree, it continues to print them one by one (Ends when there are no more contacts to print) */
    if (a) {
        /* First prints the root and then moves to the subtrees (It recalls itself changing the node to analyze) */
        printf("\nCode: %d - Name: %s - Surname: %s, Age: %d, Residence: %s\n", a->Person.Code, a->Person.Name, a->Person.Surname, a->Person.Age, a->Person.Residence);
        PrintTreeInPreorder(a->left);
        PrintTreeInPreorder(a->right);
    }
    return;
}

void PrintTreeInPostorder(TreeType a)
{
    /* As long as it finds contacts in the tree, it continues to print them one by one */
    if (a) {
        /* First moves to the subtrees and then prints the root of the tree/subtree in which it is located (It recalls itself changing the node to analyze) */
        PrintTreeInPostorder(a->left);
        PrintTreeInPostorder(a->right);
        printf("\nCode: %d - Name: %s - Surname: %s, Age: %d, Residence: %s\n", a->Person.Code, a->Person.Name, a->Person.Surname, a->Person.Age, a->Person.Residence);
    }
    return;
}

void PrintTreeInSymmetricOrder(TreeType a)
{
    /* As long as it finds contacts in the tree, it continues to print them one by one */
    if (a) {
        /* First moves to the left subtrees, then prints the root of the tree/subtree in which it is located, and then moves to the right subtrees (It recalls itself changing the node to analyze) */
        PrintTreeInSymmetricOrder(a->left);
        printf("\nCode: %d - Name: %s - Surname: %s, Age: %d, Residence: %s\n", a->Person.Code, a->Person.Name, a->Person.Surname, a->Person.Age, a->Person.Residence);
        PrintTreeInSymmetricOrder(a->right);
    }
    return;
}

void PrintTree(TreeType a, int choice)
{
    do {
        choice = PrintMenu();
        switch (choice) {
            case 1:
                printf("   Printing in preorder shortly.. \n");
                PrintTreeInPreorder(a);
                printf("\n");
                break;
            case 2:
                printf("   Printing in postorder shortly.. \n");
                PrintTreeInPostorder(a);
                printf("\n");
                break;
            case 3:
                printf("   Printing in symmetric order \n");
                PrintTreeInSymmetricOrder(a);
                printf("\n");
                break;
            case 4:
                break;
            default:
                printf("\nWrong option..try again!\n\n");
        }
    } while (choice != 4); /* I perform the switch operation until s has a value different from 4 */
    return;
}

void ExportContacts(TreeType tree, FILE *file)
{
    if (!tree) {
        /* Check if the tree is empty */
        fprintf(file, "()");
        /* If it's empty, open and close the parentheses */
        return;
    } else {
        /* If it's not empty, open a parenthesis, leave a space, and copy the contact to the file */
        fprintf(file, "( %d %s %s %d %s ", tree->Person.Code, tree->Person.Name, tree->Person.Surname, tree->Person.Age, tree->Person.Residence);
        ExportContacts(tree->left, file);
        /* Move to the left and right subtrees */
        ExportContacts(tree->right, file);
        fprintf(file, ")");
        /* Close the subtree/tree with a closing parenthesis */
    }
    return;
}

int ModifyContact(TipoAlbero *tree, int n)
{
	TipoNodo *temp;
	char str[30];
	int k, res;
}

void ExportContacts(TreeType tree, FILE *file)
{
    if (!tree) {
        /* Check if the tree is empty */
        fprintf(file, "()");
        /* If it's empty, open and close the parentheses */
        return;
    } else {
        /* If it's not empty, open a parenthesis, leave a space, and copy the contact to the file */
        fprintf(file, "( %d %s %s %d %s ", tree->Person.Code, tree->Person.Name, tree->Person.Surname, tree->Person.Age, tree->Person.Residence);
        ExportContacts(tree->left, file);
        /* Move to the left and right subtrees */
        ExportContacts(tree->right, file);
        fprintf(file, ")");
        /* Close the subtree/tree with a closing parenthesis */
    }
    return;
}

int ModifyContact(TreeType *tree, int n)
{
    NodeType *temp;
    char str[30];
    int k, ris;
    temp = SearchContact(*tree, n); /* Search the number entered in the tree to see if it exists */
    if(temp) { /* If it is present, I return the corresponding node */
        printf("\nCode: %d - Name: %s - Surname: %s, Age: %d, Residence: %s\n", temp->Person.Code, temp->Person.Name, temp->Person.Surname, temp->Person.Age, temp->Person.Residence);
        printf("\n** If you want to leave a field unchanged, write 'UNCHANGED' and press enter **\n\n"); /* For each addition, I check that there is no UNCHANGED value (If it is present, I leave the field unchanged) */
        printf("Enter the new name of the contact (Replace the space with '.' if necessary): "); /* I enter the new data of the contacts */
        scanf("%s", str);
        if(strcmp(str,"UNCHANGED")) { /* Check if equal to UNCHANGED */
            strcpy(temp->Person.Name, str); /* If different from UNCHANGED, I copy it into the struct */
        }
        printf("Enter the new surname: ");
        scanf("%s", str);
        if(strcmp(str,"UNCHANGED")) {
            strcpy(temp->Person.Surname, str);
        }
        printf("Enter the new age of the contact, '0' to leave unchanged: ");
        scanf("%d", &k);
        if(k != 0){
            temp->Person.Age = k;
        }
        printf("Enter the new residence: ");
        scanf("%s", str);
        if(strcmp(str,"UNCHANGED")) {
            strcpy(temp->Person.Residence, str);
        }
        return 1; /* I return a positive feedback */
    } else { /* If I don't find it, I exit */
        printf("\nContact Code: %d is not found in the tree\n", n);
        return 0;
    }
}

void InsertNode(TreeType *a, PersonType person, int n)
{
    if(!*a) { /* Check if the tree is empty or not */
        *a = malloc(sizeof(NodeType)); /* If it is empty, first allocate the root of the tree */
        if(!*a) {  /* Check if it is possible to allocate the node */
            printf("\nNode allocation error...sorry\n");
            return;
        }
        (*a)->Person = person; /* Copy the values just entered into the newly allocated node */
        (*a)->left = NULL; /* Being the root, for now there are no subtrees, so I put a null value in the left and right pointers */
        (*a)->right = NULL;
    } else { /* If it is not empty, I see if the number entered is less than the number of the root in the tree */
        if(n < (*a)->Person.Code){
            InsertNode(&((*a)->left), person, n); /* If so, I call the function passing to the left subtree */
        } else {
            InsertNode(&((*a)->right), person, n); /* If not, I call the function passing to the right subtree */
        } /* In this way I reorder the contacts during the insertion */
    }
    return;
}

int AddContact(TreeType *tree)
{
    NodeType *temp;
    PersonType Person;
    int n;
    printf("Enter the identification number of the new contact (>=0): ");
    scanf("%d", &n);
    if(n < 0){ /* Check that the entered number is valid */
        printf("\nThe number %d entered is not valid\n", n);
        return 0;
    }   
    temp = SearchContact(*tree, n); /* I search the entered number in the tree to see if it already exists (If it is present I exit), if it is not present I continue */
    if(temp){
        printf("\nThe contact Code. %d is already in the tree\n", n);
        return 0;
    }
    Person.Code = n; /* I copy the entered number into the contacts */
    printf("Enter the name of the contact (Replace the space with '.' if necessary): "); /* I enter the other values of the contact */
    scanf("%s", Person.Name);
    printf("Enter the surname: ");
    scanf("%s", Person.Surname);
    printf("Enter the age: ");
    scanf("%d", &(Person.Age));
    printf("Enter the residence (Max 50 characters): ");
    scanf("%s", Person.Residence);
    InsertNode(tree, Person, n); /* I call the function to insert a new node in the tree */
    NContacts += 1; /* I increment the variable containing the number of contacts allocated in the tree */
    return 1;  /* Positive feedback for successful insertion */
}

int PrintContact(TreeType Tree, int n)
{
    NodeType *temp; /* Pointer to a temporary node that I need to work with */
    temp = SearchContact(Tree, n); /* I search for the number corresponding to the node to see if it exists */
    if(temp){ /* If it does, I return the corresponding node */
        printf("\nCode: %d - Name: %s - Surname: %s, Age: %d, Residence: %s\n", temp->Person.Code, temp->Person.Name, temp->Person.Surname, temp->Person.Age, temp->Person.Residence);
        return 1; /* I return a positive feedback */
    } else {
        return 0; /* Otherwise I return a negative feedback */
    }
}

int Menu()
{
    int choice; /* User's choice */
    printf("	Contact Management Menu\n");
    printf("Contacts allocated in the program: %d\n", NContacts);
    printf("1 - Print all contacts\n");
    printf("2 - Print a contact\n");
    printf("3 - Add a contact\n");
    printf("4 - Modify a contact\n");
    printf("5 - Delete a contact\n");
    printf("6 - Delete all contacts\n");
    printf("7 - Export the tree to a file\n");
    printf("8 - Import the tree from a file\n");
    printf("9 - Close program\n");
    printf("What is your choice?: ");
    scanf("%d", &choice); /* I enter the choice and return it */
    return choice;
}

int PrintMenu()
{
    int choice;
    printf("   Contact Print Menu\n\n");
    printf("Number of contacts allocated in the program: %d\n\n", NContacts);
    printf("1 - Print in preorder\n");
    printf("2 - Print in postorder\n");
    printf("3 - Print in symmetric order\n");
    printf("4 - Back\n");
    printf("What is your choice?: ");
    scanf("%d", &choice); /* I get and return the user's choice */
    return choice;
}

NodeType *SearchContact(TreeType Tree, int n)
{
    NodeType *root, *left, *right; /* Support pointers to search for contacts in the root, right branch and left branch */
    if(Tree){ /* Check that the tree is not null */
        if(Tree->Person.Code == n){ /* I search for the entered number in the tree to see if it exists (If it is present I return it), if it is not present I continue */
            root = Tree;
            return root;
        } else {
            left = SearchContact(Tree->left, n); /* I call the function and go to scan the left and right subtrees */
            right = SearchContact(Tree->right, n);
            if(left){  
                return left; /* If I found the node on the left I return it... */
            } else {
                return right; /* ...otherwise I return the one on the right... */
            }
        }
    }
    return NULL; /* If I don't find anything I return a null value */
}

void DeleteWithTwoSubtrees(TreeType *tree, PersonType *person)
{
    NodeType *temp;
    if(*tree){
        if((*tree)->left){
            DeleteWithTwoSubtrees(&(*tree)->left, person); /* I search for the leftmost node of the right subtree of the node to be deleted (The minimum node of the right subtree will always be larger than those of the left subtree) */
        } else { /* I found the leftmost node (It can be a leaf or it will only have a right child) */
            temp = *tree; /* temp contains the node to "replace" (The one just found to the node to be deleted) */
            *person = (*tree)->Person; /* person contains the information of the found node */
            *tree = (*tree)->right; /* Tree contains the right child of the found node (It becomes the left child of the father of the found node) */
            free(temp); /* I delete the found node */
        }
    }
    return;
}

int DeleteContact(TreeType *tree, int n)
{
    NodeType *temp;
    PersonType person;
    if(*tree){
        if(n < (*tree)->Person.Code){ /* Number < tree root: deletion must be performed in the left subtree */
            DeleteContact(&(*tree)->left, n);
        } else {
            if(n > (*tree)->Person.Code){ /* Number > tree root: deletion must be performed in the right subtree */
                DeleteContact(&(*tree)->right, n);
            } else { 
                if(n == (*tree)->Person.Code){ /* I found the node to delete, consider the following cases: - The node has both subtrees not empty. - The node has a non-empty subtree and the other empty. - The root is a leaf (I can directly delete the node). */
                    if(((*tree)->left) && ((*tree)->right)){ /* The node has both subtrees not empty, to ensure the ordering of the tree I use this function that finds the minimum node in the right subtree of the node to be deleted and replaces it with the root node */
                        DeleteWithTwoSubtrees(&(*tree)->right, &person); /* I replace the root with the content of the previously found node */
                        (*tree)->Person = person;
                    } else {
                        temp = *tree; /* Node has a non-empty subtree and the other empty */
                        if(((*tree)->left)){ /* It has a left subtree, I replace the root with the left child */
                            *tree = (*tree)->left; 
                        } else {
                            if(((*tree)->right)){ /* It has a right subtree, I replace the root with the right child */
                                *tree = (*tree)->right;
                            } else { /* The root is a leaf (I can directly delete the node) */
                                *tree = NULL;
                                free(temp);
                            }
                        }
                    }
                    NContacts -= 1; /* I decrement the global NContacts */
                }
            }
        }
        return 1; /* I return a positive feedback */
    } else {
        printf("\nThe contact Code: %d is not in the tree\n", n); /* If I don't find it, I exit */
        return 0;
    }
}

void DeletionMenu(TreeType *tree, int n)
{
    NodeType *temp;
    int choice, result;  	
    temp = SearchContact(*tree, n); /* I search for the number of the corresponding node to display it */ /* I return the corresponding node */
    printf("\nCode: %d - Name: %s - Surname: %s, Age: %d, Residence: %s\n", temp->Person.Code, temp->Person.Name, temp->Person.Surname, temp->Person.Age, temp->Person.Residence);
    do {
        printf("\nAre you sure you want to delete this contact? (1 = YES / 0 = NO): ");
        scanf("%d", &choice);
        switch(choice){
            case 1:
                choice = 0;
                result = DeleteContact(tree, n);
                if(!result){
                    printf("\nDeletion not performed\n\n");
                } else {
                    printf("\nDeletion performed\n\n");
                }
                break;
            case 0:
                printf("\nGoing back\n\n");
                break;
            default:
                printf("\nWrong option\n\n");
        }
    } while(choice != 0);
    return;
}

void DeleteTree(TreeType *tree)
{
    if(*tree){ /* Check if the tree is empty or not */
        DeleteTree(&(*tree)->left); /* If it's not empty, I visit the left and right subtrees to deallocate the nodes first */
        DeleteTree(&(*tree)->right);
    }
    NContacts = 0; /* I set the NContacts of the contacts to 0 */
    free(*tree); /* I deallocate the node pointed by tree */
    *tree = NULL; /* I initialize the tree with a null value */
    return;
}

void DeallocateMenu(TreeType *tree)
{
    int choice;
    do {
        printf("Are you sure you want to delete all contacts? (1 = YES / 0 = NO): ");
        scanf("%d", &choice);
        switch(choice){
            case 1:
                DeleteTree(tree);
                choice = 0;
                printf("\nDeletion completed\n");
                break;
            case 0:
                printf("\nGoing back\n\n");
                break;
            default:
                printf("\nWrong option\n\n");
                break;
        }
    } while(choice != 0);
    return;
}

void ImportContacts(FILE *pfile,  int k, PersonType *person)
{
    person->Code = k;
    fscanf(pfile, "%s ", &(person->Name));
    fscanf(pfile, "%s ", &(person->Surname));
    fscanf(pfile, "%d ", &(person->Age));
    fscanf(pfile, "%s ", &(person->Residence));
    NContacts += 1; /* I increment the variable containing the number of contacts allocated in the tree */
    return;
}

TreeType ImportSubtrees(FILE *pfile)
{
    TreeType a = NULL;
    PersonType e;
    char car;
    int k;
    fscanf(pfile, "%c", &car); /* I read an open parenthesis */        
    fscanf(pfile, "%c", &car); /* I read the next character, a space or a closed parenthesis */ 
    if(car == ')'){ /* I see if it is a closed parenthesis, if it is I found a closed subtree */   
        return NULL;
    } else { /* Otherwise, I read a space, so I can read another contact */
        fscanf(pfile, "%d", &k);   /* I read the number of the next contact from the file */
        a = malloc(sizeof(NodeType)); /* I create another node */
        if(!a){ /* I check if the allocation went well */
            printf("\nAllocation error\n");
            return NULL;
        } else {
            ImportContacts(pfile, k, &e); /* I call the function that copies the contacts from the file to the tree */
            a->Person = e; /* I save the just read contact in the subtree */
            a->left = ImportSubtrees(pfile); /* I move to the left subtree */
            a->right = ImportSubtrees(pfile); /* I move to the right subtree */         
            fscanf(pfile, "%c", &car); /* I read the closed parenthesis */
        }
    } 
    return a; /* I return the just created subtree */
}

TreeType ImportRoot(char *FileName)
{
    TreeType tree = NULL;
    PersonType person;
    FILE *pfile;
    char car;
    int h, FileAllocated;
    pfile = fopen(FileName,"r"); /* I open the file in read mode (If there are problems, I exit) */
    if(!pfile){
        printf("\nProblem opening file\n");
        return 0;
    } else {	
        fscanf(pfile,"%d\n", &FileAllocated); /* I read the first line of the file where there is the number of contacts contained in the file */
        if(FileAllocated == 0){ /* I see if there are contacts in the file */
            printf("\nThere are no contacts in the file\n");
            fclose(pfile);	/* I close the file */
            return NULL;
        }
        fscanf(pfile,"%c",&car); /* I read an open parenthesis */         
        fscanf(pfile,"%c",&car); /* I read the next character (A space) */
        fscanf(pfile,"%d",&FileAllocated);   /* I read the number of the first contact in the file */
        tree = malloc(sizeof(NodeType)); /* I create the root */
        if(!tree){ /* I check if it is possible to allocate the node */
            printf("\nNode allocation error!\n");
            return NULL;
        } else {
            ImportContacts(pfile,h,&person); /* I call the function that imports contacts from the file to the tree */
            tree->Person = person; /* I save the just read contact in the tree */
            tree->left = ImportSubtrees(pfile); /* I move to the left subtree */
            tree->right = ImportSubtrees(pfile); /* I move to the right subtree */        
            fscanf(pfile,"%c",&car); /* I read the closed parenthesis */ 
        }
    }
    fclose(pfile);		/* I close the file */
    return tree; /* I return the just created tree */
}

void ImportMenu(TreeType *tree, char FileName[])
{
    int ncontacts;
    do {
        printf("If you import contacts from a file you will lose those previously saved.\nAre you sure you want to continue? (1 = YES / 0 = NO): ");
        scanf("%d", &ncontacts);
        switch(ncontacts) {
            case 1:
                DeleteTree(tree); /* I delete all the contacts contained in the tree */
                ncontacts = 0;
                printf("\nEnter the name of the file to import: ");
                scanf("%s", FileName);
                *tree = ImportRoot(FileName);
                if(!*tree) {
                    printf("\nImport not performed\n\n");
                } else {
                    printf("\nImport performed\n\n");
                }
                break;
            case 0:
                printf("\nGoing back\n\n");
                break;
            default:
                printf("\nWrong option\n\n");
        }
    } while(ncontacts != 0);
    return;
}