#ifndef FUNCTIONS_H
#define FUNCTIONS_H

/* Functions used in the program */
NodeType *SearchContact(TreeType, int); /* Traverses the tree comparing the input number with those contained in the tree (Returns the pointer to the found node) */
void PrintTreePreOrder(TreeType); /* Prints the entire tree in pre-order */
void PrintTreePostOrder(TreeType); /* Prints the entire tree in post-order */
void PrintTreeSymmetric(TreeType); /* Prints the entire tree symmetrically */
void PrintTree(TreeType, int); /* Choice of the type of tree printing */
int PrintContact(TreeType, int); /* Prints a contact by receiving the corresponding node from input searched via "SearchContact" */
void InsertNode(TreeType *, PersonType, int); /* Inserts a new node into the tree */
int AddContact(TreeType *); /* Adds a contact inside the tree (Returns 1 if the contact is added correctly) */
int ModifyContact(TreeType *, int); /* Modifies a contact within the tree through the number searched via "SearchContact" (Returns 1 if the modification is applied correctly) */
void DeletionWithTwoSubtrees(TreeType *, PersonType *); /* Called by ContactDeletion when both subtrees are present in the node to be deleted */
int ContactDeletion(TreeType *, int); /* Deletes a contact within the tree through the number searched via "SearchContact" (Returns 1 if the contact is deleted correctly) */
void DeleteTree(TreeType *); /* Permanently deletes all contacts (Deallocates the entire tree) */
void ExportContacts(TreeType, FILE *); /* Copies contacts from the tree to the file also traversing the subtrees and closes the tree */
int Export(TreeType, char []); /* Exports the tree to a file chosen by the user */
void ImportContacts(FILE *, int, PersonType *); /* Imports contacts from the file to the tree */
TreeType ImportRoot(char []); /* Imports the root of the tree from a file chosen by the user */
TreeType ImportSubtrees(FILE *); /* Imports the subtrees from a file chosen by the user */
void InitializeTree(TreeType *); /* Initializes the tree to null */
/* Menu for making choices */
int Menu(); /* Choice menu */
int PrintMenu(); /* Print selection menu */
void DeletionMenu(TreeType *, int); /* Menu for selecting contact deletion */
void DeallocationMenu(); /* Menu for selecting tree deallocation */
void ImportMenu(TreeType *, char []); /* Menu for selecting importation (With tree deallocation) */

#endif