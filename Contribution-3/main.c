/*Ficarola Giacomo
Problem: Personal address book management
Use of functions seen in class: Insertion, modification, deletion, deallocation of the entire tree, printing the tree in preorder, postorder, and symmetrically, export to an external file and import*/

#include <stdio.h> /*Libraries to be used*/
#include <stdlib.h>
#include <string.h>
#include "functions.h"  /*Include the header file with the function prototypes*/
#define MAX 20		/* Maximum number of characters for the file name */

/* Data structures */
struct person{ 
	int Code; 
	char Name[31];
	char Surname[31];
	int Age;
	char Residence[51];
}; /*Define the data of a person structure*/

typedef struct person PersonType;

struct Node{ /*Define the struct that will define each node of the tree, it will contain the person's information, pointers to the next nodes*/
	PersonType Person;
	struct Node *left, *right;
};

typedef struct Node NodeType;
typedef NodeType * TreeType;
/* GLOBAL VARIABLES */
int NContacts = 0; /* Keeps track of allocated contacts */

int main()
{
    TreeType *Tree = NULL; /* Define the tree and initialize it to NULL */
    char FileName[MAX];
    int choice, result, number; /* Menu choice and result of various functions */
    do {
        choice = Menu();
        switch(choice) {
            case 1:
                printf("Print all contacts\n");
                if(NContacts != 0){ /* Check if the tree is not empty */
                    PrintTree(Tree, choice);
                    printf("\n");
                } else {
                    printf("The address book is empty..\n");
                }
                break;
            case 2:
                printf("Print a contact\n");
                if(NContacts != 0){  /* Check if the tree is not empty */
                    printf("Enter the contact number: ");
                    scanf("%d", &number);
                    if(number < 0){  /* Check if the contact number is valid, if it is, proceed */
                        printf("\nThe number %d is not valid\n",number);
                    } else {
                        result = PrintContact(Tree, number);
                        if(!result){ /* If the result is different from 0, return an error */
                            printf("\n\nThe contact with Code: %d is not in the tree\n",number);
                        }
                    }
                } else {
                    printf("The address book is empty..\n");
                }
                break;
            case 3:
                printf("Add a contact\n");
                result = AddContact(&Tree);
                if(!result){
                    printf("\nAddition not performed\n");
                } else {
                    printf("\nAddition performed\n");
                }
                break;
            case 4:
                printf("Modify a contact\n");
                if(NContacts != 0){  /* Check if the tree is not empty */
                    PrintTreeInPreorder(Tree);
                    printf("Enter the contact number to modify: ");
                    scanf("%d", &number);
                    if(number < 0){ /* Check if the contact number is valid, if it is, proceed */
                        printf("\nThe number %d is not valid\n",number);
                    } else {
                        result = ModifyContact(&Tree,number);
                        if(!result){
                            printf("\nContact modification not performed\n");
                        } else {
                            printf("\nContact modification performed\n");
                        }
                    }
                } else {
                    printf("The address book is empty..\n");
                }
                break;
            case 5:
                printf("Delete a contact\n");
                if(NContacts != 0){  /* Check if the tree is not empty */
                    PrintTreeInPreorder(Tree);
                    printf("Enter the contact number to delete: ");
                    scanf("%d", &number);
                    if(number < 0){  /* Check if the contact number is valid, if it is, proceed */
                        printf("\nThe number %d is not valid\n",number);
                    } else {
                        MenuDeletion(&Tree,number);
                    }
                } else {
                    printf("The address book is empty..\n");
                }
                break;
            case 6:
                printf("Delete all contacts\n");
                if(NContacts != 0){ /* Check if the tree is not empty */
                    MenuDeallocation(&Tree);
                } else {
                    printf("The address book is empty..\n");
                }	  
                break;
            case 7:
                printf("Export the tree\n");
                if(NContacts != 0){  /* Check if the tree is not empty */
                    printf("Enter the name of the file to create: ");
                    scanf("%s", FileName);
                    result = Export(Tree,FileName);
                    if(!result){
                        printf("\nExport not performed\n");
                    } else {
                        printf("\nExport performed\n");
                    }
                } else {
                    printf("The address book is empty..\n");
                }
                break;
            case 8:
                printf("Import tree\n");
                if(NContacts != 0){
                    MenuImport(&Tree,FileName);
                } else {
                    printf("Enter the name of the file to import: ");
                    scanf("%s", FileName);
                    Tree = ImportRoot(FileName);
                    if(!Tree){
                        printf("\nImport not performed\n");
                    } else {
                        printf("\nImport performed\n");
                    }
                }
                break;	  	
            case 9:
                printf("\nProgram closed, thank you and see you next time!\n");
                break;
            default:
                printf("\nWrong option\n");
        }
    } while(choice != 9);
    system("PAUSE");
    return 0;
}