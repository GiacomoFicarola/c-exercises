#ifndef FUNCTIONS_H
#define FUNCTIONS_H

/* Function prototypes that will be used in the program */
void DeleteList(ListType *);
int CheckEmptyList(ListType);
void InitializeList(ListType *);
unsigned int Choice();
int Print(ListType, int);
void InsertAtHead(ListType *);
void ChangeTime(ListType *, int);
void InsertAtTail(ListType *);
void SaveToFile(ListType, int);
void Delete (ListType *, int);
void LoadFromFile(ListType *, int *);
void BookSeats(ListType *, int);

#endif