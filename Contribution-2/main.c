/* Program designed for the management of buses for the company Cotral S.P.A. The program consists of various structs, among which I have identified
 those for the time of each individual bus. This is composed of hours and minutes, and I have typedefed it as TipoOra. Another struct is that of the
 individual bus composed of a code, destination, departure time, and available seats on board, which can be reserved upon request, perhaps by paying in advance
 for the seat online (let's indulge in fantasy). Last but not least is the struct that defines the node, both of the latter two are typedefed ad hoc.
 The program consists of 9 functions plus the main, totaling 10.
 These functions handle printing any received values, managing the user menu, inserting a bus at the head or tail, deleting a bus by search,
 reserving seats on a bus after searching for it, resulting in a reduction of available seats, changing the departure time and minutes of a bus,
 importing a list of buses from a previously exported file, and exporting the entire list of buses in memory to disk.
 Ficarola Giacomo */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "functions.h"

/* Data structure definitions */
struct Time { 
        int Hour;
        int Minutes;
};

typedef struct Time TimeType;

struct Bus { 
    char Code[6];
    char Destination[31];
    TimeType Schedule;
    int FreeSeats;
}; 

typedef struct Bus BusType;

struct Node { 
        BusType Bus;
        struct Node *next;
};

typedef struct Node NodeType;

typedef NodeType * ListType;

int BusCount = 0; /* Value to count the buses inserted in the list */

int main(void)
{
     int p=1;
     TipoLista list; /*Initialize the list by defining it as TipoLista type*/
     InitializeList(&list);
     p = CheckEmptyList(list);
     if(p == 0){
          printf("The list contains something\n");
     } else {
          printf("The list is empty\n");
     }
     unsigned int choice; /*Declare a variable to store and check the choice made in the choice void function*/
     while((choice = choose()) != 10){
          switch(choice){ /*Handle all the choice cases*/
               case 1:
                    print(list, NBus); /*Print the bus list on the screen*/
                    break;
               case 2:
                    InsertAtHead(&list); /*Add a new bus at the head*/
                    NBus += 1; /*Increment the number of buses inserted in the list*/
                    break;
               case 3:
                    InsertAtTail(&list);
                    NBus += 1; /*Increment the number of buses inserted in the list*/
                    break;
               case 4:
                    delete(&list, NBus); /*Delete a bus*/
                    NBus -= 1;
                    break;
               case 5:
                    BookSeats(&list, NBus); /*Book seats on a bus*/
                    break;
               case 6:
                    ChangeTime(&list, NBus); /*Change departure time and minutes*/
                    break;
               case 7:
                    LoadFromFile(&list, &NBus); /*Import the bus list from a file*/
                    break;
               case 8:
                    SaveToFile(list, NBus); /*Export the bus list to a file*/
                    break;
               case 9:
                    DeleteList(&list); /*Delete the entire list from the last node to the first and free all the nodes*/
                    break;
               default:
                    puts("\nIncorrect choice!"); /*Unexpected choice*/
                    break;
          }
     }
     printf("\nThank you and goodbye!\n\n");
     system("PAUSE");
     return 0;
}