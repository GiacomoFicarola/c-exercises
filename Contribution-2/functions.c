#include "functions.h"

void DeleteList(ListType *list)
{
     if (*list != NULL) {
          DeleteList(&(*list)->next);
          free(list);
          *list = NULL;
          BusCount -= 1;
     }
     return;
}

int CheckEmptyList(ListType list)
{
     if(list){
          return 0;
     }else{
          return 1;
     }
}

void InitializeList(ListType *list)
{
     *list = NULL;
     return;
}

unsigned int Choice(void) {
     /* Prints the menu of options that the user will see */
     unsigned int chosen; /* Declares the number that will be selected by the user */
     printf("%s","\nWelcome to the Cotral s.p.a bus management\nChoose one of the following options:\n"  /* Prints all the possible options for the user */
             "1 - Print all buses\n"
             "2 - Add a bus at the beginning\n"
             "3 - Add a bus at the end\n"
             "4 - Delete a bus from the list\n"
             "5 - Book seats on a bus\n"
             "6 - Change bus time\n"
             "7 - Load from a file\n"
             "8 - Export to a file\n"
             "9 - Delete the entire bus list\n"
             "10 - Terminate the program\n"
             "What would you like to do :) ?");
     scanf("%u", &chosen);
     return chosen;
}

int Print(ListType list, int N) /* I am going to print the content of the list! */
{
     if (N == 0) { /* Check if there are any buses in the list, if not, print nothing */
          printf("\nOh, but there are no buses, what did you expect me to print?!?\n\n");
          return 1; /* return 1 to handle the error */
     } else {
          printf("\nNow we will print all the buses!\n\n");
          while (list != NULL) {
               printf("Bus %s, for %s, will depart at %d:%d, has %d free seats\n", list->Bus.Cod, list->Bus.Destinazione, list->Bus.Orario.Ora, list->Bus.Orario.Minuti, list->Bus.PostiLiberi);
               list = list->next;
          } /* Print the bus list to the screen */
          return 0;
     }
}

void InsertAtHead(ListType *list) /* inserts elem into the list; list is the address of the pointer variable that points to the beginning of the list */
{
     NodeType *newNode;
     newNode = malloc(sizeof(NodeType)); /* allocate new node */
     if (newNode == NULL) { /* if allocation is successful, insert the new node at the head of the list */
          printf("Allocation of the node FAILED..\n");
     } else {
          printf("Inserting Bus data\n\nEnter the code (max 5): "); /* fill the new node with input data */
          scanf("%s", newNode->Bus.Code);
          printf("Enter the destination of the Bus: ");
          scanf("%s", newNode->Bus.Destination);
          printf("Enter the departure hour of the bus: ");
          scanf("%d", &(newNode->Bus.Time.Hour));
          printf("Enter the minutes: ");
          scanf("%d", &(newNode->Bus.Time.Minutes));
          printf("Enter the number of available seats: ");
          scanf("%d", &(newNode->Bus.FreeSeats));
          newNode->next = *list; /* new node above the first node in the list, insert the address of the first node (head of the list) into the next field */
          *list = newNode; /* Now the new node is the first node in the list, *list contained the address of the head of the list, but now it has become a secondary node, instead of the address of the second node, I put the address of the first node, which is the new one */
          printf("\nBus inserted successfully at the head!\n");
     }
     return;
}

void ChangeTime(ListType *list, int N) /* Function needed to change the departure time and minutes of a specific bus */
{
     int var; /* Local variables to get time, minutes and number, in addition to the index */
     char code[6];
     NodeType *aux;
     aux = *list; /* I equate the head of the list to aux */
     printf("\nWelcome to the section for changing a bus's time.\n");
     if (print(*list, N) == 0) {
          printf("Enter the code of the flight you want to change the time of: ");
          scanf("%s", code); /* I get the bus identification code */
          while (aux != NULL) { /* I scroll aux */
               if (strcmp(code, aux->Bus.Code) == 0) { /* I find it.. */
                    printf("Enter the new departure time: "); /* I ask for the new time */
                    scanf("%d", &(aux->Bus.Schedule.Hour)); /* I change it */
                    printf("Time change made!\n");
                    printf("Do you also want to change the minutes? (0=Yes, 1=No): "); /* I ask if he wants to change something else */
                    scanf("%d", &var);
                    if (var == 0) {
                         printf("Enter the new departure minutes: "); /* I ask for the new minutes.. */
                         scanf("%d", &(aux->Bus.Schedule.Minutes)); /* I change them */
                         printf("Minutes change made!\n");
                    }
               } else {
                    aux = aux->next;
               }
               printf("Bus not found..\n"); /* If the code is not found... */
          }
     }
     return;
}

void InsertAtTail(ListType *list)
{
     NodeType *previous, *current, *newNode;
     /* previous points to the first node in the list */
     previous = *list;
     /* current points to the first node in the list */   
     current = *list;
    /* While current is not NULL */
     while(current != NULL){
         /* If I haven't exited the loop: previous points to current and current points to the next node */
         previous = current;
         current = current->next;
     }
    /* At this point, previous points to the predecessor of the new node and current points to the successor of the new node */
    /* I create a new node to add to the list */
    newNode = malloc(sizeof(NodeType));
    /* I check if it is possible to allocate the node */
    if(!newNode){
        printf("\nAllocation error\n");
        return;
    }
    printf("Let's enter the Bus data\nEnter the code (max 5): "); /* filling new node with info taken from input*/
    scanf("%s",newNode->Bus.Code);
    printf("Enter the Bus destination: ");
    scanf("%s",newNode->Bus.Destination);
    printf("Enter the bus departure time: ");
    scanf("%d",&(newNode->Bus.Schedule.Hour));
    printf("Enter the minutes: ");
    scanf("%d",&(newNode->Bus.Schedule.Minutes));
    printf("Enter the number of seats on board: ");
    scanf("%d",&(newNode->Bus.FreeSeats));
    /* previous will point to the node just added to the list */
    previous->next = newNode;
    /* newNode will point to its successor (Pointed by current) */
    newNode->next = current;
    return;
}

void SaveToFile(ListType list, int numBuses) /* I receive the list and the number of buses */
{
     char fileName[51]; /* I declare the array to contain the chosen file name */
     printf("Enter the name of the file on which you want to export the entire Bus list (Max 50, extension at your choice):");
     scanf("%s", fileName);
     FILE *file; /* I create the file pointer */
     file = fopen(fileName, "w"); /* I open the file for writing */
     if(file == NULL) {
          printf("\nProblem opening file\n"); /* I report the problem in opening the file */
          return;
     } else {
          /* I write in the first line of the file the number of buses allocated so far */
          fprintf(file, "%d\n", numBuses);
          while(list != NULL) {
               fprintf(file, "%s ", list->Bus.Code); /* I print the individual nodes contained in the list on the file */
               fprintf(file, "%s ", list->Bus.Destination);
               fprintf(file, "%d ", list->Bus.Schedule.Hour);
               fprintf(file, "%d ", list->Bus.Schedule.Minutes);
               fprintf(file, "%d\n", list->Bus.FreeSeats);
               list = list->next;
          }
          printf("\nPrinting on file done!\n");
     }
     fclose(file); /* I close the file pointer */
     return;
}

void Delete(TipoLista *list, int N) /* Function needed to delete a specific element */
{
     char toDelete[6]; /* String containing the code to search for */
     TipoNodo *pgen, *curr, *prev; /* Declaration of pointers to list, working with them on list */
     curr = prev = pgen = malloc(sizeof(TipoNodo)); /* Memory allocation for pgen */
     if (pgen == NULL) {
          printf("Memory allocation for pgen failed...\n"); /* If allocation fails, exit */
          return;
     }
     pgen->next = *list; /* Assign pgen the head of the list */
     prev = pgen;
     curr = *list; /* curr will contain the address where the list starts, it will hold the field occupied with the address of the list */
     printf("Welcome to the section dedicated to deleting a bus from the list.\n");
     if (print(*list, N) == 0) { /* Print the list only if the print function returns 0, meaning it has some content, if 1 it has nothing */
          printf("\nPlease enter the code of the bus you want to delete (Max 5): ");
          scanf("%s", toDelete); /* Ask for the code */
          while (curr != NULL) {
               if (strcmp(toDelete, curr->Bus.Code) == 0) { /* If found.. */
                    prev->next = curr->next;
                    free(curr);
                    *list = pgen->next;
                    free(pgen);
                    printf("Deletion successful, see you next time!\n");
               }
               prev = prev->next;
               curr = curr->next;
          }
     }
     return;
}

void LoadFromFile(TipoLista *list, int *NBus)
{
     int i, AllocatedInFile, n = 0;
     char FileName[51]; /* Array to ask for the file name */
     TipoNodo *List, *New;
     List = *list; /* Assign the address of the first node to List, work on List and keep list */
     FILE *file; /* Pointer of type FILE, to the file */
     printf("Enter the name of the file from which to load the bus list (Max 50): ");
     scanf("%s", FileName);
     file = fopen(FileName, "r"); /* Open the file in read mode */
     if (file == NULL) {
          printf("\nProblem opening file...\n"); /* Exit and close everything */
          return; /* Close everything */
     } else {
          /* Read the first line of the file where the number of buses in the file is stored */
          fscanf(file, "%d\n", &AllocatedInFile);
          if (AllocatedInFile == 0) { /* Check if there are buses in the file */
               printf("\nThere are no buses in the file\n");
               return;
          } else {
               if (List == NULL) { /* If there is a direct import as soon as the program starts, it says it is impossible */
                    printf("The list is completely empty, insert at least one element before importing others!\n");
                    return;
               } else {
                    while (List->next != NULL) {
                         List = List->next; /* Move through the list until the last node */
                    }
                    for (i = 0; i < AllocatedInFile; i++) { /* Loop through the number written in the file header, insert everything at the end of the running list */
                         New = malloc(sizeof(TipoNodo)); /* Allocate the new node with the TipoNodo struct */
                         List->next = New; /* In the next field of the last node, insert the address of the new node */
                         fscanf(file, "%s ", &(New->Bus.Code)); /* Perform all the insertions at the end of the list */
                         fscanf(file, "%s ", &(New->Bus.Destination));
                         fscanf(file, "%d ", &(New->Bus.Schedule.Hour));
                         fscanf(file, "%d ", &(New->Bus.Schedule.Minutes));
                         fscanf(file, "%d\n", &(New->Bus.FreeSeats));
                         New->next = NULL; /* Close the list after inserting the new node by setting the next field to NULL */
                         List = List->next;
                         *NBus = *NBus + 1;
                    }
               }
          }
     }
     fclose(file); /* Close the file */
     return;
}

void BookSeats(ListType *, int)
{
     char code[6];
     int seats;
     NodeType *aux;
     aux = *list;
     printf("\nWelcome to the section dedicated to booking seats on a bus.\n");
     if (print(*list, N) == 0) {
          printf("Enter the code of the bus on which you want to book seats (Max 5): ");
          scanf("%s", code);
          while (aux != NULL) {
               if (strcmp(code, aux->Bus.Code) == 0) {
                    printf("Enter the number of seats you want to book: ");
                    scanf("%d", &seats);
                    if (seats > aux->Bus.FreeSeats) {
                         printf("There are not enough seats available, try again later!\n");
                    } else {
                         aux->Bus.FreeSeats -= seats;
                         printf("Seats booked successfully!\n");
                    }
               } else {
                    aux = aux->next;
               }
               printf("Bus not found..\n");
          }
     }
     return;
}
